<?php

/**
 * @file
 * Feeds DSpace parser
 */

/**
 * Class definition for DSpace Parser.
 *
 * Parses DSpace REST API feed.
 */
class FeedsDSpaceParser extends FeedsParser {

  /**
   * Implements FeedsParser::parse().
   */
  public function parse(FeedsSource $source, FeedsFetcherResult $fetcher_result) {
    $items = json_decode($fetcher_result->getRaw());
    $result = new FeedsParserResult();

    foreach ($items as $item) {
      $publication['guid'] = $item->handle;
      $publication['id'] = $item->id;
      $publication['lastmodified'] = $item->lastModified;
      $publication['title'] = $item->name;

      // Dublin Core Metadata.
      if (isset($item->metadata)) {
        foreach ($item->metadata as $metadata) {
          switch ($metadata->key) {
            case 'dc.type':
              $publication['type'] = $metadata->value;
              break;
            case 'dc.identifier.uri':
              $publication['identifier'] = $metadata->value;
              break;
            case 'dc.relation.ispartofseries':
              $publication['number'] = $metadata->value;
              break;
            case 'dc.contributor.author':
              $publication['authors'][] = $metadata->value;
              break;
            case 'dc.subject':
              $publication['keywords'][] = $metadata->value;
              break;
            case 'dc.description.abstract':
              $publication['abstract'] = $metadata->value;
              break;
            case 'dc.date.issued':
              $publication['issued'] = $metadata->value;
              break;
            case 'dc.identifier.citation':
              $publication['citation'] = $metadata->value;
              break;
          }
        }
      }

      $result->items[] = $publication;
      unset($publication);
    }

    return $result;
  }

  /**
   * Implements FeedsParser::getMappingSources().
   */
  public function getMappingSources() {
    return array(
      'guid' => array(
        'name' => t('GUID'),
        'description' => t('Global Unique Identifier of the feed item.'),
      ),
      'id' => array(
        'name' => t('ID'),
        'description' => t('Identifier of the feed item.'),
      ),
      'lastmodified' => array(
        'name' => t('Last Modified'),
        'description' => t('Last Modified of the feed item.'),
      ),
      'title' => array(
        'name' => t('Title'),
        'description' => t('Title of the feed item.'),
      ),
      'submitter' => array(
        'name' => t('Submitter'),
        'description' => t('Submitter of the feed item.'),
      ),
      'type' => array(
        'name' => t('Type'),
        'description' => t('Publication type of the feed item.'),
      ),
      'identifier' => array(
        'name' => t('Identifier'),
        'description' => t('Identifier of the feed item.'),
      ),
      'number' => array(
        'name' => t('Number'),
        'description' => t('Number of the feed item.'),
      ),
      'authors' => array(
        'name' => t('Authors'),
        'description' => t('Authors of the feed item.'),
      ),
      'keywords' => array(
        'name' => t('Keywords'),
        'description' => t('Keywords of the feed item.'),
      ),
      'abstract' => array(
        'name' => t('Abstract'),
        'description' => t('Abstract of the feed item.'),
      ),
      'issued' => array(
        'name' => t('Issued'),
        'description' => t('Issue Year of the feed item.'),
      ),
      'citation' => array(
        'name' => t('Citation'),
        'description' => t('Bibliographic citation of the feed item.'),
      ),
    ) + parent::getMappingSources();
  }
}
